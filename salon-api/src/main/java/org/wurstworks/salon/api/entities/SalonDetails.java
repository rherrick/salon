package org.wurstworks.salon.api.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

@Data
@NoArgsConstructor
public class SalonDetails {
    @Value("${salon.name}")
    private String name;
    @Value("${salon.address}")
    private String address;
    @Value("${salon.city}")
    private String city;
    @Value("${salon.state}")
    private String state;
    @Value("${salon.zipcode}")
    private String zipcode;
    @Value("${salon.phone}")
    private String phone;
}
