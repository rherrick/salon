package org.wurstworks.salon.api.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class Slot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long                     id;
    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<SalonServiceDetails> availableServices;
    @ManyToOne
    private SalonServiceDetails      selectedService;
    private SlotStatus               status;
    private String                    stylistName;
    private LocalDateTime             slotFor;
    private LocalDateTime             lockedAt;
    private LocalDateTime             confirmedAt;
}

