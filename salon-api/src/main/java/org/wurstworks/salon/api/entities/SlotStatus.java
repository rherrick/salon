package org.wurstworks.salon.api.entities;

enum SlotStatus {
    AVAILABLE, LOCKED, CONFIRMED, CANCELLED
}
