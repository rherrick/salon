package org.wurstworks.salon.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.wurstworks.salon.api.entities.SalonServiceDetails;

public interface SalonServiceDetailsRepository extends JpaRepository<SalonServiceDetails, Long> {
}
