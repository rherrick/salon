package org.wurstworks.salon.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.wurstworks.salon.api.entities.Slot;

public interface SlotRepository extends JpaRepository<Slot, Long> {
}
