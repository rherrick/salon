import "./App.css"
import Modal from "react-bootstrap/Modal"
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
    return (
        <div className="App">
            <Modal.Dialog>
                <Modal.Header>
                    <Modal.Title>AR Salon and Day Spa Services</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>This is some text.</p>
                </Modal.Body>
            </Modal.Dialog>
        </div>
    )
}

export default App
